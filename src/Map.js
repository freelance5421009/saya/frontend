import React, { useEffect, useState } from 'react';
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet';
import L from 'leaflet';
import 'leaflet/dist/leaflet.css';

const Map = () => {
  const [busLocations, setBusLocations] = useState([]);
  const astanaPosition = [51.1694, 71.4491]; // Astana city coordinates

  useEffect(() => {
    const fetchBusLocations = async () => {
      try {
        const response = await fetch('http://52.224.71.86:5000/tracking/last');
        if (!response.ok) {
          throw new Error('Failed to fetch bus locations');
        }
        const data = await response.json();
        // Transforming data into the expected format
        const locations = Object.values(data).map(({ lat, lon, name }) => ({
          name,
          lat,
          lon,
        }));
        setBusLocations(locations);
      } catch (error) {
        console.error('Error fetching bus locations:', error);
      }
    };

    const interval = setInterval(fetchBusLocations, 3000);

    return () => clearInterval(interval);
  }, []);

  // Creating a custom marker icon
  const customMarkerIconInstance = L.icon({
    iconUrl: process.env.PUBLIC_URL + '/pininamap_87198.svg',
    iconSize: [32, 32], // Marker image size
    iconAnchor: [16, 32], // Anchor for positioning the marker
  });

  return (
    <MapContainer center={astanaPosition} zoom={12} style={{ height: '600px', width: '100%' }} scrollWheelZoom={false}>
      <TileLayer
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
      />
      {busLocations.map((bus, index) => (
        <Marker key={index} position={[bus.lat, bus.lon]} icon={customMarkerIconInstance}>
          <Popup>{bus.name}</Popup>
        </Marker>
      ))}
    </MapContainer>
  );
};

export default Map;
