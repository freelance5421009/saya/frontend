import React from 'react';
import Map from './Map';

const App = () => {
  return (
    <div>
      <h1>Bus Tracking Map</h1>
      <Map />
    </div>
  );
};

export default App;
